function send()
{
    jQuery.ajax(
    {
        url : window.location.pathname,
        type : "POST",
        data :  $('#main-form').serialize()
    });
    show();
}

function send2(counter, x)
{
    var element = document.getElementById('h' + counter);
    element.value = x;
    jQuery.ajax(
    {
        url : window.location.pathname,
        type : "POST",
        data :  $('#button-form').serialize()
    });
    show();
}

function send3(counter, x)
{
    var element = document.getElementById('f1' + counter);
    element.value = x;
    jQuery.ajax(
    {
        url : window.location.pathname,
        type : "POST",
        data :  $('#floor-form1').serialize()
    });
    show();
}

function show()
{
    jQuery.ajax({
        url: window.location.pathname,
        cache: false,
        success: function(html){
            jQuery("#ajax_data").html(html);
        }
    });
}

jQuery(document).ready(function(){
    setInterval('show()', 7000);
});


function change(x) {
    var element = document.getElementById('scenario_change');
    element.value = x;
    document.forms["changeForm"].submit();
}


function plus(x) {
    var element = document.getElementById('fl2' + x);
    element.value = parseInt(element.value) + 1;
    send();
}

function minus(x) {
    var element = document.getElementById('fl2' + x);
    element.value = parseInt(element.value) - 1;
    send()
}
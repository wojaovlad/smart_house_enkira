function submit_form() {
    document.forms["graphicsForm"].submit();
}

function delete_graphic(x) {
    if (confirm("Удалить график?")) {
        var element = document.getElementById('delete_graphic');
        element.value = x;
        document.forms["deleteGraphicForm"].submit();
    }
}

function magic_with_data(graphics_data, position) {
    var new_data = [];
    var x = [];
    for (var i = 0; i < graphics_data.length; ++i) {
        x = (graphics_data[i]).slice();
        x.splice(position, 1);
        new_data.push(x);
    }
    return new_data;
}


function submit_form() {
    document.forms["scenarioForm"].submit();
}

function delete_scenario(x) {
    if (confirm("Удалить сценарий?")) {
    var element = document.getElementById('delete_scenario');
    element.value = x;
    document.forms["deleteScenarioForm"].submit();
    }
}

function change(x) {
    var element = document.getElementById('scenario_change');
    element.value = x;
    document.forms["changeForm"].submit();
}

function click_condition() {
    var element = document.getElementById('room_condition');
    element.style.display = 'inline';
}
# coding=utf-8
from __future__ import unicode_literals
from django.db import models


class Room(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name

    class Meta:
        managed = True
        db_table = 'room'


class DataSensors(models.Model):
    room_key = models.ForeignKey(Room)
    foto_light = models.IntegerField(db_column='Foto_Light', unique=True)
    pir_sensor = models.CharField(db_column='Pir_sensor', max_length=40)
    gas_sensor = models.IntegerField(db_column='Gas_sensor')
    information = models.CharField(blank=True, default='data sensor', max_length=50)
    on_the_main = models.BooleanField(blank=True, default=False)

    def __unicode__(self):
        return self.information

    class Meta:
        managed = True
        db_table = 'data_sensors'


class OutSensor(models.Model):
    temperature_out = models.CharField(max_length=40)
    humidity_out = models.CharField(max_length=40)
    on_the_main = models.BooleanField(blank=True, default=False)

    class Meta:
        managed = True
        db_table = 'out_sensor'


class Dht11(models.Model):
    description = models.CharField(max_length=35)
    pin1 = models.PositiveIntegerField()
    pin2 = models.PositiveIntegerField()
    on_the_main = models.BooleanField(blank=True, default=False)

    def __unicode__(self):
        return self.description


class Dht(models.Model):
    room_key = models.ForeignKey(Room)
    description = models.TextField(db_column='Description')
    temperature = models.CharField(max_length=255)
    humidity = models.CharField(max_length=155)
    temperature_new = models.CharField(max_length=255)
    pin_number = models.PositiveIntegerField()
    on_the_main = models.BooleanField(blank=True, default=False)

    def __unicode__(self):
        return self.description

    class Meta:
        managed = True
        db_table = 'dht'


class DjangoMigrations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = True
        db_table = 'django_migrations'


class Pindescription(models.Model):
    room_key = models.ForeignKey(Room)
    pinnumber = models.CharField(db_column='pinNumber', unique=True, max_length=2)  # Field name made lowercase.
    pindescription = models.CharField(db_column='pinDescription', max_length=255)  # Field name made lowercase.

    def __unicode__(self):
        return self.pindescription

    class Meta:
        managed = True
        db_table = 'pindescription'


class Pindirection(models.Model):
    room_key = models.ForeignKey(Room)
    pinnumber = models.CharField(db_column='pinNumber', unique=True, max_length=2)  # Field name made lowercase.
    pindirection = models.CharField(db_column='pinDirection', max_length=3)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'pindirection'


class Pinstatus(models.Model):
    room_key = models.ForeignKey(Room)
    data_sensor_key = models.ManyToManyField(DataSensors)
    pindescription = models.TextField(db_column='PinDescription')  # Field name made lowercase.
    pinnumber = models.CharField(db_column='pinNumber', unique=True, max_length=2)  # Field name made lowercase.
    pinstatus = models.CharField(db_column='pinStatus', max_length=1)  # Field name made lowercase.
    pinstatusweb = models.CharField(db_column='PinStatusWeb', max_length=1)  # Field name made lowercase.
    on_the_main = models.BooleanField(blank=True, default=False)

    def __unicode__(self):
        return self.pindescription

    class Meta:
        managed = True
        db_table = 'pinstatus'


class Users(models.Model):
    username = models.CharField(unique=True, max_length=28)
    password = models.CharField(max_length=64)
    salt = models.CharField(max_length=8)

    class Meta:
        managed = True
        db_table = 'users'


class GraphicData(models.Model):
    date_time = models.DateTimeField()
    temperature = models.FloatField()
    humidity = models.FloatField()

    def __unicode__(self):
        return str(self.date_time)

    class Meta:
        managed = True
        ordering = ['date_time']


class GraphicObject(models.Model):
    name = models.CharField(max_length=30)
    TEMPERATURE = 'TM'
    HUMIDITY = 'HM'
    DATE = 'DT'
    GRAPHIC_DATA_CHOICES = (
        (TEMPERATURE, 'Temperature'),
        (HUMIDITY, 'Humidity'),
        (DATE, 'Date')
    )
    axis_x = models.CharField(max_length=2, choices=GRAPHIC_DATA_CHOICES, default=TEMPERATURE)
    axis_y = models.CharField(max_length=2, choices=GRAPHIC_DATA_CHOICES, default=TEMPERATURE)
    type = models.CharField(max_length=3, choices=(('DAY', 'За сутки'), ('ALL', 'За все время')))
    on_the_main = models.BooleanField(blank=True, default=False)

    def __unicode__(self):
        return self.name


class Scenario(models.Model):
    name = models.CharField(max_length=50)
    active = models.BooleanField(default=False)
    timer = models.IntegerField(help_text="Задержка выполнения, с")
    on_the_main = models.BooleanField(blank=True, default=False)

    def __unicode__(self):
        return self.name


class ScenarioConditionChoice(models.Model):
    type = models.CharField(max_length=25)
    interface_text = models.CharField(max_length=35)

    def __unicode__(self):
        return self.interface_text


class ScenarioActionChoice(models.Model):
    type = models.CharField(max_length=25)
    interface_text = models.CharField(max_length=35)

    def __unicode__(self):
        return self.interface_text


class WetFloor(models.Model):
    description = models.CharField(max_length=50)
    room_key = models.ForeignKey(Room)
    status = models.IntegerField()
    temperature = models.FloatField()
    temperature_new = models.FloatField()
    on_the_main = models.BooleanField(blank=True, default=False)

    def __unicode__(self):
        return self.room_key.name


class Widget(models.Model):
    name = models.CharField(max_length=30)
    on_the_main = models.BooleanField(blank=True, default=False)
    interface_text = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class Notification(models.Model):
    text = models.CharField(max_length=50)
    date_time = models.DateTimeField(auto_now=True)
    new = models.BooleanField(default=True)
    scenario_key = models.ForeignKey(Scenario)
    type = models.CharField(max_length=10)

    def __unicode__(self):
        return self.text


class OtherSensors(models.Model):
    room_key = models.ForeignKey(Room, blank=True)
    info = models.CharField(max_length=30, blank=True)
    MOTION = 'MS'
    DISTANCE = 'DS'
    SENSOR_TYPE_CHOICES = (
        (MOTION, 'Датчик движения'),
        (DISTANCE, 'Датчик расстояния')
    )
    type = models.CharField(max_length=2, choices=SENSOR_TYPE_CHOICES, default=MOTION)
    value = models.IntegerField()


class Instruments(models.Model):
    room_key = models.ForeignKey(Room, blank=True)
    info = models.CharField(max_length=30, blank=True)
    SOCKET = 'MS'
    ENGINE = 'DS'
    INSTRUMENT_TYPE_CHOICES = (
        (SOCKET, 'Розетка'),
        (ENGINE, 'Двигатели')
    )
    type = models.CharField(max_length=2, choices=INSTRUMENT_TYPE_CHOICES, default=SOCKET)
    value = models.IntegerField()


class ScenarioCondition(models.Model):
    scenario_key = models.ForeignKey(Scenario)
    condition_choice_key = models.ForeignKey(ScenarioConditionChoice)
    room_key = models.ForeignKey(Room, blank=True, null=True)
    arg = models.IntegerField(blank=True, default=0)

    def __unicode__(self):
        return self.scenario_key.name


class ScenarioAction(models.Model):
    scenario_key = models.ForeignKey(Scenario)
    action_choice_key = models.ForeignKey(ScenarioActionChoice)
    room_key = models.ForeignKey(Room, blank=True, null=True)
    arg = models.IntegerField(blank=True, default=0)

    def __unicode__(self):
        return self.scenario_key.name
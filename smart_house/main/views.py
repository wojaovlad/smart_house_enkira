# coding=utf-8
from django.shortcuts import render, get_object_or_404, render_to_response, HttpResponseRedirect
from django.core.urlresolvers import reverse
from main.models import *
import time


def data_for_graphics():
    graphics_data = GraphicData.objects.all()
    all_data = []
    for data in graphics_data:
        all_data.append([int(time.mktime(data.date_time.timetuple())), data.temperature, data.humidity])
    return all_data


def make_light_query(data):
    light = []
    for sensor in data:
        for x in DataSensors.objects.filter(pinstatus=sensor):
            if x.pir_sensor == '1':
                setattr(sensor, 'status', 'on')
                light.append(sensor)
                break
        else:
            setattr(sensor, 'status', 'off')
            light.append(sensor)
    return light


def index(request):
    if request.POST:
        if 'action' in request.POST:
            light_sensors = Pinstatus.objects.all()
            value = request.POST['action']
            for sensor in light_sensors:
                sensor.pinstatusweb = value
                sensor.save()
        if 'scenario_change' in request.POST:
            change_scenario = get_object_or_404(Scenario, pk=int(request.POST['scenario_change']))
            if change_scenario.active:
                change_scenario.active = False
            else:
                change_scenario.active = True
            change_scenario.save()
        for data in request.POST:
            print data
            if data[:3] == 'dht':
                dht_sensor = get_object_or_404(Dht, pk=data[3:])
                dht_sensor.temperature_new = request.POST[data]
                dht_sensor.save()
            if data[:3] == 'lht':
                light_sensor = get_object_or_404(Pinstatus, pk=data[3:])
                light_sensor.pinstatusweb = request.POST[data]
                light_sensor.save()
            elif data[:3] == 'fl1':
                floor = get_object_or_404(WetFloor, pk=data[3:])
                floor.status = int(request.POST[data])
                floor.save()
            elif data[:3] == 'fl2':
                floor = get_object_or_404(WetFloor, pk=data[3:])
                floor.temperature_new = int(request.POST[data])
                floor.save()
    rooms = Room.objects.all()
    out_sensor = OutSensor.objects.all()
    main_light = Pinstatus.objects.filter(on_the_main=True)
    main_dht = Dht.objects.filter(on_the_main=True)
    main_scenarios_active = Scenario.objects.filter(on_the_main=True, active=True)
    main_scenarios_inactive = Scenario.objects.filter(on_the_main=True, active=False)
    main_graphics = GraphicObject.objects.filter(on_the_main=True)
    main_data_sensors = DataSensors.objects.filter(on_the_main=True)
    main_floor = WetFloor.objects.filter(on_the_main=True)
    main_widgets = Widget.objects.filter(on_the_main=True)
    graphics_data = False
    if main_graphics:
        graphics_data = data_for_graphics()
    light = False
    if main_light:
        light = make_light_query(main_light)

    if request.is_ajax():
        return render(request, 'index_ajax.html', {
            'rooms': rooms,
            'out_sensor': out_sensor,
            'dht': main_dht,
            'active_scenarios': main_scenarios_active,
            'inactive_scenarios': main_scenarios_inactive,
            'graphics': main_graphics,
            'data_sensors': main_data_sensors,
            'graphics_data': graphics_data,
            'light': light,
            'main_floor': main_floor,
            'main_widgets': main_widgets
        })

    return render(request, 'index.html', {
        'rooms': rooms,
        'out_sensor': out_sensor,
        'dht': main_dht,
        'active_scenarios': main_scenarios_active,
        'inactive_scenarios': main_scenarios_inactive,
        'graphics': main_graphics,
        'data_sensors': main_data_sensors,
        'graphics_data': graphics_data,
        'light': light,
        'main_floor': main_floor,
        'main_widgets': main_widgets
    })


def room_detail(request, room_id):
    if request.POST:
        print request.POST
        for data in request.POST:
            if data[:3] == 'dht':
                dht_sensor = get_object_or_404(Dht, pk=data[3:])
                dht_sensor.temperature_new = request.POST[data]
                dht_sensor.save()
            elif data[:3] == 'lht':
                if int(request.POST[data]) != 100:
                    light_sensor = get_object_or_404(Pinstatus, pk=data[3:])
                    light_sensor.pinstatusweb = request.POST[data]
                    light_sensor.save()
            elif data[:3] == 'fl1':
                floor = get_object_or_404(WetFloor, pk=data[3:])
                floor.status = int(request.POST[data])
                floor.save()
            elif data[:3] == 'fl2':
                floor = get_object_or_404(WetFloor, pk=data[3:])
                floor.temperature_new = int(request.POST[data])
                floor.save()
    out_sensor = OutSensor.objects.all()
    current_room = get_object_or_404(Room, pk=room_id)
    dht = current_room.dht_set.all()
    rooms = Room.objects.all()
    data_sensors = current_room.datasensors_set.all()
    room_light = current_room.pinstatus_set.all()
    light = make_light_query(room_light)
    wet_floor = current_room.wetfloor_set.all()

    if request.is_ajax():
        return render(request, 'room_ajax.html', {
            'light': light,
            'dht': dht,
            'out_sensor': out_sensor,
            'current_room': current_room,
            'data_sensors': data_sensors,
            'wet_floor': wet_floor
        })

    return render(request, 'room.html', {
        'light': light,
        'rooms': rooms,
        'current_room': current_room,
        'dht': dht,
        'out_sensor': out_sensor,
        'data_sensors': data_sensors,
        'wet_floor': wet_floor
        })


def graphics(request):
    if request.POST:
        graphic = GraphicObject(axis_x=request.POST['axis_x'], axis_y=request.POST['axis_y'],
                                name=request.POST['graphic_name'], type=request.POST['type'])
        graphic.save()
    rooms = Room.objects.all()
    graphic_objects = GraphicObject.objects.all()

    return render(request, 'graphics.html', {
        'choices': {'DT': 'Дата', 'TM': 'Температура', 'HM': 'Влажность'},
        'graphics': graphic_objects,
        'graphics_data': data_for_graphics(),
        'rooms': rooms
    })


def scenarios(request):
    if request.POST:
        try:
            request.POST['scenario_change']
        except Exception:
            try:
                active = request.POST['is_active']
            except Exception:
                active = False
            else:
                active = True

            scenario = Scenario(name=request.POST['scenario_name'], timer=request.POST['timer'], active=active)
            scenario.save()
            print request.POST
            for data in request.POST:
                if data == 'cnd':
                    condition = ScenarioCondition(scenario_key=get_object_or_404(Scenario, pk=scenario.id),
                                                  condition_choice_key=get_object_or_404(ScenarioConditionChoice,
                                                                                         pk=request.POST[data]),
                                                  room_key=get_object_or_404(Room, pk=request.POST['room_condition']))
                    condition.save()
                elif data == 'act':
                    action = ScenarioAction(scenario_key=get_object_or_404(Scenario, pk=scenario.id),
                                            action_choice_key=get_object_or_404(ScenarioActionChoice,
                                                                                pk=request.POST[data]))
                    action.save()
        else:
            change_scenario = get_object_or_404(Scenario, pk=int(request.POST['scenario_change']))
            if change_scenario.active:
                change_scenario.active = False
            else:
                change_scenario.active = True
            change_scenario.save()
    active_scenarios = Scenario.objects.filter(active=True)
    inactive_scenarios = Scenario.objects.filter(active=False)
    rooms = Room.objects.all()
    actions, conditions = ScenarioActionChoice.objects.all(), ScenarioConditionChoice.objects.all()

    return render(request, 'scenarios.html', {
        'active_scenarios': active_scenarios,
        'inactive_scenarios': inactive_scenarios,
        'rooms': rooms,
        'actions': actions,
        'conditions': conditions
    })


def graphic_delete(request):
    if request.POST:
        graphic = get_object_or_404(GraphicObject, pk=request.POST["delete"])
        graphic.delete()
    return HttpResponseRedirect(reverse('graphics'))


def scenario_delete(request):
    if request.POST:
        scenario = get_object_or_404(Scenario, pk=request.POST["delete"])
        scenario.delete()
    return HttpResponseRedirect(reverse('scenarios'))


def settings_index(request):
    if request.POST:
        object1 = False
        for data in request.POST:
            if data == 'graph':
                object1 = get_object_or_404(GraphicObject, pk=request.POST['graph'])
            elif data == 'light':
                object1 = get_object_or_404(Pinstatus, pk=request.POST['light'])
            elif data == 'scenario':
                object1 = get_object_or_404(Scenario, pk=request.POST['scenario'])
            elif data == 'data_sensor':
                object1 = get_object_or_404(DataSensors, pk=request.POST['data_sensor'])
            elif data == 'dht':
                object1 = get_object_or_404(Dht, pk=request.POST['dht'])
            elif data == 'floor':
                object1 = get_object_or_404(WetFloor, pk=request.POST['floor'])
            elif data == 'widget':
                object1 = get_object_or_404(Widget, pk=request.POST['widget'])
        if object1.on_the_main:
            object1.on_the_main = False
        else:
            object1.on_the_main = True
        object1.save()

    all_widgets = Widget.objects.all()
    graphic_objects = GraphicObject.objects.all()
    light_sensors = Pinstatus.objects.all()
    all_scenarios = Scenario.objects.all()
    data_sensors = DataSensors.objects.all()
    rooms = Room.objects.all()
    all_dht = Dht.objects.all()
    all_floor = WetFloor.objects.all()

    return render(request, 'settings_index.html', {
        'rooms': rooms,
        'graphic_objects': graphic_objects,
        'light_sensors': light_sensors,
        'all_scenarios': all_scenarios,
        'data_sensors': data_sensors,
        'all_dht': all_dht,
        'all_floor': all_floor,
        'all_widgets': all_widgets
    })


def notifications(request):
    if request.is_ajax():
        del_notification = get_object_or_404(Notification, pk=request.POST['scenario_id'])
        del_notification.delete()
    else:
        all_notifications = Notification.objects.order_by('-new')
        return render(request, 'notifications.html', {
            'all_notifications': all_notifications
        })
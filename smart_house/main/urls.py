from django.conf.urls import patterns, url
from main import views

urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^room/(?P<room_id>\d+)/$', views.room_detail, name='room_detail'),
    url(r'^graphics$', views.graphics, name='graphics'),
    url(r'^scenarios$', views.scenarios, name='scenarios'),
    url(r'^scenario_delete$', views.scenario_delete, name='scenario_delete'),
    url(r'^graphic_delete$', views.graphic_delete, name='graphic_delete'),
    url(r'^settings_index$', views.settings_index, name='settings_index'),
    url(r'^notifications$', views.notifications, name='notifications'),
)

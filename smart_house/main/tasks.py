from celery.task import periodic_task
from datetime import timedelta, datetime
import urllib
from xml.dom import minidom
from django.shortcuts import get_object_or_404
from main.models import *

wurl = 'http://xml.weather.yahoo.com/forecastrss?p=%s'
wser = 'http://xml.weather.yahoo.com/ns/rss/1.0'


def weather_for_zip(zip_code):
    url = wurl % zip_code + '&u=c'
    dom = minidom.parse(urllib.urlopen(url))
    forecasts = []
    for node in dom.getElementsByTagNameNS(wser, 'forecast'):
        forecasts.append({
            'date': node.getAttribute('date'),
            'low': node.getAttribute('low'),
            'humidity': node.getAttribute('humidity'),
            'high': node.getAttribute('high'),
            'condition': node.getAttribute('text')
        })
    ycondition = dom.getElementsByTagNameNS(wser, 'condition')[0]
    yatmosphere = dom.getElementsByTagNameNS(wser, 'atmosphere')[0]
    return {
        'current_condition': ycondition.getAttribute('text'),
        'current_temp': ycondition.getAttribute('temp'),
        'humidity': yatmosphere.getAttribute('humidity'),
        'forecasts': forecasts,
        'title': dom.getElementsByTagName('title')[0].firstChild.data
    }


@periodic_task(run_every=timedelta(minutes=10))
def main():
    a = weather_for_zip("BOXX0005")
    temp = a['current_temp']
    humidity = a['humidity']
    out_sensor = get_object_or_404(OutSensor, pk=1)
    out_sensor.humidity_out = humidity
    out_sensor.temperature_out = temp
    out_sensor.save()


@periodic_task(run_every=timedelta(seconds=30))
def check():
    active_scenarios = Scenario.objects.filter(active=True)
    if active_scenarios:
        for scenario in active_scenarios:
            status = True
            room = False
            for condition in scenario.scenariocondition_set.all():
                if condition.condition_choice_key.type == 'lightOff':
                    room_light = Pinstatus.objects.filter(room_key=condition.room_key)
                    for light in room_light:
                        if light.pinstatus != '0':
                            status = False
                            for notification in Notification.objects.filter(scenario_key=scenario):
                                notification.new = False
                                notification.save()
                            break
                    room = condition.room_key
            else:
                for notification in Notification.objects.filter(scenario_key=scenario):
                    if notification.new:
                        status = False
            if status:
                for action in scenario.scenarioaction_set.all():
                    if action.action_choice_key.type == 'addNotification':
                        notification = Notification(text=room, new=True, scenario_key=scenario, date_time=datetime,
                                                    type='light')
                        notification.save()

